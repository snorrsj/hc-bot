# bot.py
import os
import discord
import random
import praw
import youtube_dl


from dotenv import load_dotenv
from discord.ext import commands

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
GUILD = os.getenv('DISCORD_GUILD')
images = os.path.join(os.path.dirname(__file__), 'images/')
images_list = [images + img for img in os.listdir(images)]

client = discord.Client()
bot = commands.Bot(command_prefix='!')

reddit = praw.Reddit(client_id = 'k8owdhT_GQQKmQ',
                    client_secret = 'LqNTrikQOf5_3iwT5UKLX488-w7J2w',
                    user_agent = 'Hot Corner Bot by u/snorresj)',
                    check_for_async=False)


@bot.event
async def on_ready():
    print(f'{bot.user.name} has connected to Discord!')


@bot.command(name='hjelp')
async def hjelp(ctx):
    response = "Make me do shit:\n\nFor en dank meme: '!meme'\nFor guttastemning: '!gutta'\n\nKommandoer for avspilling av musikk:\n'!spill <youtube-link>\n'!pause'\n'!stopp'\n'!gjenoppta'\n'!forlat'"
    await ctx.send(response)


@bot.command(name='meme')
async def meme(ctx):
    subreddit = reddit.subreddit('dankmemes')
    all_subs = []
    top = subreddit.hot(limit=50)
    for submission in top:
        all_subs.append(submission)
    random_sub = random.choice(all_subs)
    name = random_sub.title
    url = random_sub.url
    em = discord.Embed(title=name)
    em.set_image(url=url)
    await ctx.send(embed=em)


@bot.command(name='gutta')
async def gutta(ctx):
    await ctx.send(file=discord.File((random.choice(images_list))))



########## MUSIKK

@bot.command(name='spill')
async def spill(ctx, url:str):
    song_there = os.path.isfile('song.mp3')

    try:
        if song_there:
            os.remove('song.mp3')
    except PermissionError:
        await ctx.send('Vent til musikken er ferdig avspilt eller bruk kommandoen "!stopp"')
        return
    except ClientException:
        await discord.utils.get(bot.voice_clients, guild=ctx.guild).disconnect()

    voiceChannel = discord.utils.get(ctx.guild.voice_channels, name='kokka')
    await voiceChannel.connect()
    voice = discord.utils.get(bot.voice_clients, guild=ctx.guild)

    ydl_options = {
        'format' : 'bestaudio/best',
        'postprocessors' : [{
            'key' : 'FFmpegExtractAudio',
            'preferredcodec' : 'mp3',
            'preferredquality' : '192'
        }],
    }
    with youtube_dl.YoutubeDL(ydl_options) as ydl:
        ydl.download([url])

    for file in os.listdir('./'):
        if file.endswith('.mp3'):
            os.rename(file, 'song.mp3')


    voice.play(discord.FFmpegPCMAudio('song.mp3'))
    ##song = discord.FFmpegPCMAudio('song.mp3')
    ##song.play()


@bot.command(name='forlat')
async def forlat(ctx):
    voice = discord.utils.get(bot.voice_clients, guild=ctx.guild)
    if voice.is_connected():
        await voice.disconnect()
    else:
        await ctx.send('Jeg er ikke i noen voice channel.')


@bot.command(name='pause')
async def pause(ctx):
    voice = discord.utils.get(bot.voice_clients, guild=ctx.guild)
    if voice.is_playing():
        voice.pause()
    else:
        await ctx.send('Det spilles ikke av noe musikk.')


@bot.command(name='gjenoppta')
async def gjenoppta(ctx):
    voice = discord.utils.get(bot.voice_clients, guild=ctx.guild)
    if voice.is_paused():
        voice.resume()
    else:
        await ctx.send('Musikken er ikke pauset.')


@bot.command(name='stopp')
async def stopp(ctx):
    voice = discord.utils.get(bot.voice_clients, guild=ctx.guild)
    if voice.is_connected():
        voice.stop()
        voice.disconnect()
    else:
        await ctx.send('Jeg er ikke i noen voice channel.')


bot.run(TOKEN)
